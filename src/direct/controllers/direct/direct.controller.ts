import { Controller, Get, Query, Res } from '@nestjs/common';
import { Response } from 'express';
import { DirectService } from '../../aggregates/direct/direct.service';

@Controller('direct')
export class DirectController {
  constructor(private readonly direct: DirectService) {}

  @Get('connect')
  connectFrappe(@Query('redirect') redirect, @Res() res: Response) {
    this.direct.connectClientForUser(redirect).subscribe({
      next: success => res.redirect(success.redirect),
      error: ({ error }) => {
        res.status(500);
        res.json({ message: error.message });
      },
    });
  }

  @Get('callback')
  oauth2callback(
    @Res() res: Response,
    @Query('state') state: string,
    @Query('code') code: string,
  ) {
    this.direct.oauth2callback(res, state, code);
  }
}
