import { Injectable, HttpService } from '@nestjs/common';
import { settingsAlreadyExists } from '../../../constants/exceptions';
import { ServerSettings } from '../../../system-settings/entities/server-settings/server-settings.entity';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import {
  PROFILE_ENDPOINT,
  AUTH_ENDPOINT,
  TOKEN_ENDPOINT,
  REVOKE_ENDPOINT,
} from '../../../constants/app-strings';

@Injectable()
export class SetupService {
  constructor(
    protected readonly serverSettingsService: ServerSettingsService,
    protected readonly http: HttpService,
  ) {}

  async setup(params) {
    if (await this.serverSettingsService.count()) {
      throw settingsAlreadyExists;
    }

    const settings = new ServerSettings();
    Object.assign(settings, params);
    settings.profileURL = settings.appURL + PROFILE_ENDPOINT;
    settings.authServerURL = settings.appURL + AUTH_ENDPOINT;
    settings.tokenURL = settings.appURL + TOKEN_ENDPOINT;
    settings.revocationURL = settings.appURL + REVOKE_ENDPOINT;

    await settings.save();
  }

  async getInfo() {
    const info = await this.serverSettingsService.find();
    if (info) {
      info._id = undefined;
      info.serviceAccountSecret = undefined;
    }
    return info;
  }
}
