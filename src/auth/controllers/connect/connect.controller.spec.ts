import { Test, TestingModule } from '@nestjs/testing';
import { TokenCacheService } from '../../entities/token-cache/token-cache.service';
import { ConnectController } from './connect.controller';
import { ConnectService } from '../../aggregates/connect/connect.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';

describe('ConnectController', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [ConnectController],
      providers: [
        {
          provide: TokenCacheService,
          useValue: {},
        },
        {
          provide: ConnectService,
          useValue: {},
        },
        {
          provide: ServerSettingsService,
          useValue: {},
        },
      ],
    })
      .overrideGuard(FrappeWebhookGuard)
      .useValue({})
      .compile();
  });
  it('should be defined', () => {
    const controller: ConnectController = module.get<ConnectController>(
      ConnectController,
    );
    expect(controller).toBeDefined();
  });
});
