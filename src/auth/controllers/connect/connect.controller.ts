import { Controller, Post, Body, UseGuards } from '@nestjs/common';
import { TokenCacheService } from '../../entities/token-cache/token-cache.service';
import { FrappeWebhookGuard } from '../../guards/frappe-webhook.guard';
import { FrappeBearerTokenWebhookInterface } from '../../entities/token-cache/frappe-bearer-token-webhook.interface';
import { ConnectService } from '../../aggregates/connect/connect.service';

@Controller('connect')
export class ConnectController {
  constructor(
    private readonly tokenCacheService: TokenCacheService,
    private readonly connectService: ConnectService,
  ) {}

  @Post('v1/token_delete')
  @UseGuards(FrappeWebhookGuard)
  async tokenDelete(@Body('accessToken') accessToken) {
    await this.tokenCacheService.deleteMany({ accessToken });
  }

  @Post('v1/token_added')
  @UseGuards(FrappeWebhookGuard)
  async userDelete(@Body() payload: FrappeBearerTokenWebhookInterface) {
    return await this.connectService.createFrappeBearerToken(payload);
  }
}
