import { Injectable } from '@nestjs/common';
import { TokenCacheService } from '../../entities/token-cache/token-cache.service';
import { FrappeBearerTokenWebhookInterface } from '../../entities/token-cache/frappe-bearer-token-webhook.interface';
import { TokenCache } from '../../entities/token-cache/token-cache.entity';
import * as uuidv4 from 'uuid/v4';

@Injectable()
export class ConnectService {
  constructor(private readonly tokenCacheService: TokenCacheService) {}

  async createFrappeBearerToken(
    frappeBearerToken: FrappeBearerTokenWebhookInterface,
  ) {
    const token = await this.tokenCacheService.findOne({
      email: frappeBearerToken.user,
    });
    if (!token) {
      const tokenObject = new TokenCache();
      const mappedToken: TokenCache = this.mapFrappeBearerToken(
        frappeBearerToken,
        tokenObject,
      );
      this.tokenCacheService.save(mappedToken);
      return;
    }
    this.tokenCacheService.updateOne(
      { email: frappeBearerToken.user },
      { $set: { accessToken: frappeBearerToken.access_token } },
    );
    return;
  }

  mapFrappeBearerToken(
    frappeBearerToken: FrappeBearerTokenWebhookInterface,
    tokenObject: TokenCache,
  ): TokenCache {
    tokenObject.email = frappeBearerToken.user;
    tokenObject.status = frappeBearerToken.status;
    tokenObject.exp = 3600;
    tokenObject.uuid = uuidv4();
    tokenObject.accessToken = frappeBearerToken.access_token;
    tokenObject.refreshToken = frappeBearerToken.refresh_token;
    tokenObject.scope = frappeBearerToken.scopes.split(' ');

    const now = new Date().getTime() / 1000;
    tokenObject.exp = now + Number(frappeBearerToken.expires_in);
    return tokenObject;
  }
}
